'use strict';

angular.module('materialBootstrap', [])
    .directive('bootstrapSwitch', function(){
        return {
            restrict: 'E',
            replace: true,
            require:'ngModel',
            template: '<div class="togglebutton pull-right"><label><input type="checkbox"></label></div>',
            link: function (scope, element, attrs, ngModelCtrl) {
                // Инициализируем material bootstrap код
                $.material.init();

                // реализация метода ngModelCtrl.$render — вызывается, когда происходит обновление модели, указываемой в ngModel
                ngModelCtrl.$render = function(){
                    scope.model = ngModelCtrl.$viewValue;
                };

                // Смотрим на значение ngModel, что бы визуально показать состояние переключателя
                scope.$watch('model', function (value) {
                    // console.log('scope.model: ', value);
                    if (value) {
                        scope.label = 'Устройство включено';
                        $(element).find('input').prop('checked', true);
                    } else {
                        scope.label = 'Устройство выключено';
                        $(element).find('input').prop('checked', false);
                    }
                });

                // При изменени значения checkbox
                element.on('change', function() {
                    scope.toggle();
                });

                // При нажатии на пробел
                element.on('keydown', function(e) {
                    var key = e.which ? e.which : e.keyCode;
                    if (key === 32) {
                        scope.$apply(scope.toggle);
                    }
                });

                // Если переключатель не заблокирован, меняем значение модели
                scope.toggle = function toggle() {
                    if(!scope.disabled) {
                        scope.model = !scope.model;
                        // Меняем значение модели на противоположное
                        ngModelCtrl.$setViewValue(scope.model);
                    }
                };
            }
        }
    })
    .directive('bootstrapButton', function(){
        return {
            restrict: 'E',
            replace: true,
            require:'ngModel',
            template: '<button type="button" class="btn btn-raised btn-default btn-sm">{{text}}</button>',
            link: function (scope, element, attrs, ngModelCtrl) {
                // реализация метода ngModelCtrl.$render — вызывается, когда происходит обновление модели, указываемой в ngModel
                ngModelCtrl.$render = function(){
                    scope.model = ngModelCtrl.$viewValue;
                };

                // Смотрим на значение ngModel, что бы визуально показать состояние переключателя
                scope.$watch('model', function (value) {
                    // console.log('bootstrapButton scope.model: ', value);
                    // $(element).prop('disabled', false);
                    if (value) {
                        scope.text = 'Деактивировать';
                        $(element).removeClass('btn-success').addClass('btn-default');
                    } else {
                        scope.text = 'Активировать';
                        $(element).removeClass('btn-default').addClass('btn-success');
                    }
                });

                // При нажатии на кнопку
                element.on('click', function() {
                    // scope.toggle();
                    // $(element).prop('disabled', true);
                });

                // При нажатии на пробел
                // element.on('keydown', function(e) {
                //     var key = e.which ? e.which : e.keyCode;
                //     if (key === 32) {
                //         scope.$apply(scope.toggle);
                //     }
                // });

                // Если переключатель не заблокирован, меняем значение модели
                scope.toggle = function toggle() {
                    if(!scope.disabled) {
                        scope.model = !scope.model;
                        // Меняем значение модели на противоположное
                        ngModelCtrl.$setViewValue(scope.model);
                    }
                };
            }
        }
    });