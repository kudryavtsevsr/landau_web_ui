'use strict';

angular.module('landauApp')
    // Фильтр для форматирования вида элементов по их типу в виджете теплицы
    .filter('typeFilter', function ($sce) {
        return function (value, type) {
            switch (type) {
                case "humidity":
                    return value + " %, RH";
                    break;
                case "temperature":
                    return value + " °C";
                    break;
                case "switch":
                    return value ?
                        $sce.trustAsHtml('<span class="label label-success">On</span>') :
                        $sce.trustAsHtml('<span class="label label-info">Off</span>');
                    break;
                default:
                    return value;
            }
        };
    })

    // Фильтр выбирает контролы совпадающие по типу со слотом шаблона виджета
    .filter('metaTypeFilter', function () {
        return function (items, search, widgets, templateUid, widget, action) {
            var result = [];
            var template = null;
            if (action == 'edit') {
                template = widget.template;
            } else {
                template = templateUid;
            }
            angular.forEach(items, function (value, key) {
                if (value['metaType'] === search) {
                    // Если тип контрола switch, добавляем только реле
                    if (search == "switch") {
                        if(/^EXT.+/i.test(value['name'])) result.push(value);
                    } else {
                        result.push(value);
                    }
                } else if (search === undefined) {
                    result.push(value);
                }
            });

            // ----- Так как есть правла, использующие одни и те же устройства, пока заклмментируем -----
/*
            // Если фильтр применяется для правила
            if (/_rule/.test(template)) {
                // Исключаем из списка контролов занятые реле
                if (search == "switch") {
                    // Проходим по существующим виджетам
                    for (var key in widgets) {
                        if (action == 'edit' && key == widget.uid) continue; // Исключаем редактируемый виджет
                        var item = widgets[key];
                        // Определяем, какие реле уже используются в правилах
                        if(/_rule/.test(item.template)) {
                            var controls = item.controls;
                            for (var key2 in controls) {
                                var slot = item.controls[key2];
                                // Выделяем реле
                                if (/^EXT.+/i.test(slot.topic.name)) {
                                    // Удаляем занятое реле
                                    for (var i = 0; i < result.length; i++) {
                                        if(result[i].name == slot.topic.name) {
                                            result.splice(i, 1);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
*/
            return result;
        };
    })
    .filter('orderObjectBy', function () {
        return function (input, attribute) {
            if (!angular.isObject(input)) return input;

            var array = [];
            for (var objectKey in input) {
                array.push(input[objectKey]);
            }

            array.sort(function (a, b) {
                a = parseInt(a[attribute]);
                b = parseInt(b[attribute]);
                return a - b;
            });
            return array;
        }
    });