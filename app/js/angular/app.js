'use strict';

angular.module('landauApp', [
    'ngRoute',
    'ngSanitize',
    'ui.bootstrap',
    'materialBootstrap',
    'wu.masonry'])
    .config(function ($routeProvider, $locationProvider) {
        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('');
        $routeProvider
            .when('/', {
                templateUrl: 'views/greenhouses.html',
                controller: 'GreenhousesCtrl'
            })
            .when('/greenhouse/:id', {
                templateUrl: 'views/greenhouse.html',
                controller: 'GreenhouseCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });
    })
    .run([function () {
        $.material.init();
    }]);