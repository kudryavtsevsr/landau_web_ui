"use strict";

angular.module("landauApp")
  .factory("RuleSettingsProxy", [ "MqttRpc", function (MqttRpc) {
    return MqttRpc.getProxy("rules/test", [
      "addScript",
      "delScript"
    ], "editorProxy");
  }]);
