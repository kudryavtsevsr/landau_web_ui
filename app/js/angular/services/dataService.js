'use strict';

angular.module('landauApp')
  .factory('HomeUIData', ['$window', function($window) {
    var data = { devices:{}, controls:{}, widgets:{}, widget_templates:{}, rooms:{}, dashboards:{}, defaults: {} , parameters: {}};
    var dataService = {};
    var globalPrefix = '';

    data.widget_templates = {
        temperature: {
            uid: 'temperature', name: 'Температура',
            options: {},
            slots: {
                slot0: {name: 'Датчик температуры', uid: 'slot0', type: 'temperature'}
            }
        },
        humidity: {
            uid: 'humidity', name: 'Влажность',
            options: {},
            slots: {
                slot0: {name: 'Датчик влажности', uid: 'slot0', type: 'rel_humidity'}
            }
        },
        switch: { uid: 'switch', name: 'Вкл/Выкл',
            options: {},
            slots: {
                slot0: { name: 'Устройство', uid: 'slot0', type: 'switch' }
            }
        },
        // Правила автоматизации
        air_humidity_rule : { uid: 'air_humidity_rule', name: 'Правило контроля влажности воздуха',
            options : {
                option1: { name: 'Минимальная влажность', uid: 'option1', value: '' },
                option2: { name: 'Максимальная влажность', uid: 'option2', value: '' }
            },
            slots : {
                slot1: { name: 'Датчик влажности', uid: 'slot1', type: 'rel_humidity' },
                slot2: { name: 'Осушитель', uid: 'slot2', type : 'switch' },
                slot3: { name: 'Увлажнитель', uid: 'slot3', type : 'switch' }
            }
        },
        air_temperature_rule : { uid: 'air_temperature_rule', name: 'Правило контроля температуры воздуха',
            options : {
                option1: { name: 'Минимальная температура', uid: 'option1', value: '' },
                option2: { name: 'Максимальная температура', uid: 'option2', value: '' },

                option3: { name: 'Минимальная влажность', uid: 'option3', value: '' },
                option4: { name: 'Максимальная влажность', uid: 'option4', value: '' }
            },
            slots : {
                slot1: { name: 'Датчик температуры', uid: 'slot1', type: 'temperature' },

                slot2: { name: 'Обогреватель', uid: 'slot2', type : 'switch' },
                slot3: { name: 'Вытяжка', uid: 'slot3', type : 'switch' },
                slot4: { name: 'Туманообразователь', uid: 'slot4', type : 'switch' },

                slot5: { name: 'Датчик влажности', uid: 'slot5', type: 'rel_humidity' }
            }
        },
        illumination_by_sensor_rule: {  uid: 'illumination_by_sensor_rule', name: 'Правило контроля освещённости',
            options : {
                option1: { name: 'Минимальная освещённость', uid: 'option1', value: '' },
                option2: { name: 'Начало контроля освещённости', uid: 'option2', value1: '',value2: '', type: 'time'  },
                option3: { name: 'Окончание контроля освещённости', uid: 'option3', value1: '',value2: '', type: 'time' }
            },
            slots : {
                slot1: { name: 'Датчик освещённости', uid: 'slot1', type: 'lux' },
                slot2: { name: 'Устройство освещения', uid: 'slot2', type: 'switch' }
            }
        },
        soil_moisture_rule: {  uid: 'soil_moisture_rule', name: 'Правило мониторинга влажности почвы',
            options : {
                option1: { name: 'Минимальная влажность почвы', uid: 'option1', value: '' },
                option2: { name: 'Максимальная влажность почвы', uid: 'option2', value: ''}
            },
            slots : {
                slot1: { name: 'Датчик влажности почвы', uid: 'slot1', type: 'rel_humidity' } // Заменить на тип дптчика влажности почвы
            }
        },
        soil_temperature_rule: {  uid: 'soil_temperature_rule', name: 'Правило контроля температуры почвы',
            options : {
                option1: { name: 'Минимальная температура почвы', uid: 'option1', value: ''},
                option2: { name: 'Максимальная температура почвы', uid: 'option2', value: ''}
            },
            slots : {
                slot1: { name: 'Датчик температуры почвы', uid: 'slot1', type: 'temperature'},
                slot2: { name: 'Обогреватель почвы', uid: 'slot2', type: 'switch'}
            }
        },
        watering_by_schedule_rule: {  uid: 'watering_by_schedule_rule', name: 'Правило полива по расписанию',
            options : {
                option1: { name: 'Объём воды в литрах на каждое сопло', uid: 'option1', value: ''},
                option2: { name: 'Производительность одного сопла системы полива (литры/час)', uid: 'option2', value: ''},
                option3: { name: 'Дата начала полива', uid: 'option3', value1: '',value2: '', value3: '', type: 'date'},
                option4: { name: 'Дата окончания полива', uid: 'option4', value1: '',value2: '', value3: '', type: 'date'},
                option5: { name: 'Время начала полива', uid: 'option5', value1: '',value2: '', type: 'time'},
                option6: { name: 'Частота полива в днях', uid: 'option6', value: ''}
            },
            slots : {
                slot1: { name: 'Система полива', uid: 'slot1', type: 'switch'}
            }
        }
    };

    dataService.parseMsg = function(message) {
      if($window.localStorage['prefix'] === 'true') globalPrefix = '/client/' + $window.localStorage['user'];
      var pathItems = message.destinationName.replace(globalPrefix, '').split('/');
        parseMsg(pathItems, message);

      // console.log('======================');
      // console.log(data);
      // console.log('======================');
    };

    dataService.list = function() {
      return data;
    };

    dataService.addDevice = function(uid, device) {
      data.devices[uid] = device;
    };

    function parseMsg(pathItems, message){
      switch(pathItems[1]) {
        case "devices":
          parseDeviceMsg(pathItems, message);
          break;
        case "config":
          parseConfigMsg(pathItems, message);
          break;
        default:
          // console.log("WARNING: Unknown message: " + pathItems[1]);
          return null;
          break;
      }
    }

    function parseDeviceMsg(pathItems, message){
      var device = {};
      var deviceName = pathItems[2];
      if(data.devices[deviceName] != null){ // We already register the device, change it
        device = data.devices[deviceName];
      }else {
        device = {name: deviceName, controls: {}};
        dataService.addDevice(deviceName, device);
      }
      parseDeviceInfo(pathItems, message);
    }

    function parseDeviceInfo(pathItems, message){
      switch(pathItems[3]) {
        case "meta":
          parseDeviceMeta(pathItems, message);
          break;
        case "controls":
          parseControls(pathItems, message);
          break;
      }
    }

    function parseDeviceMeta(pathItems, message){
      var deviceName = pathItems[2];
      data.devices[deviceName]['meta' + capitalizeFirstLetter(pathItems[4])] = message.payloadString;
    }

    function capitalizeFirstLetter(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function parseControls(pathItems, message){
      var deviceName = pathItems[2];
      var controlName = pathItems[4];
      var topic = pathItems.slice(0,5).join('/');
      var control = {};

      if(data.controls[topic] != null) {
        control = data.controls[topic];
      } else {
        control = data.controls[topic] = {name: controlName, value: 0};
      }

      control.topic = topic;

      switch(pathItems[5]) {
        case "meta":
          parseControlMeta(pathItems, message);
          break;
        case undefined:
          var value = message.payloadString;
          //FIXME: implement proper parsing of value based on meta type?
          if (message.payloadString == "1") {
              value = 1;
          } else if (message.payloadString == "0") {
              value = 0;
          }
          control.value = value;
      }

      data.devices[deviceName].controls[controlName] = control;
    }

    function parseControlMeta(pathItems, message){
      var topic = pathItems.slice(0,5).join('/');
      var value = message.payloadString;
      var metaName = pathItems[6];

      if (metaName == 'order') {
		  value = parseInt(value);
	  }

      data.controls[topic]['meta' + capitalizeFirstLetter(metaName)] = value;
    }

    function parseConfigMsg(pathItems, message){
      switch(pathItems[2]) {
        case "widgets":
          parseWidgetMsg(pathItems, message);
          break;
        case "rooms":
          parseRoomMsg(pathItems, message);
          break;
        case "dashboards":
          parseDashboardMsg(pathItems, message);
          break;
        case "default_dashboard":
          data.defaults["dashboard"] = message.payloadString;
          break;
        case "parameters":
          parseParameters(pathItems, message);
          console.log('parameters: ' + message.payloadString);
          break;
        default:
          console.log("WARNING: Unknown config message: " + pathItems[2]);
          return null;
          break;
      };
    }

    function parseWidgetMsg(pathItems, message){
      var deviceInfo = message.payloadString.split('/');
      var deviceName = deviceInfo[2];
      var controlName = deviceInfo[4];
      var widgetUID = pathItems[3];
      var widget = {controls: {}, options: {}};

      if(data.widgets[widgetUID] != null){
        widget = data.widgets[widgetUID];
      } else {
		if (message.payloadString == "") {
			return;
		}
        widget['uid'] = widgetUID;
      }

      if(pathItems[4] === 'controls'){
        widget.controls[pathItems[5]] = widget.controls[pathItems[5]] || {};
        switch(pathItems[6]) {
          case "uid":
            widget.controls[pathItems[5]]['uid'] = message.payloadString;
            break;
          case "topic":

            var device = data.devices[deviceName];
            if (device != undefined) {
				widget.controls[pathItems[5]]['topic'] = device.controls[controlName];
			} else {
				widget.controls[pathItems[5]]['topic'] = undefined;
			}

            break;
          default:
            console.log("WARNING: Unknown control message: " + pathItems[6]);
            return null;
            break;
        };
      }else if(pathItems[4] === 'options'){
        widget.options[pathItems[5]] = widget.options[pathItems[5]] || {};
        widget.options[pathItems[5]][pathItems[6]] = message.payloadString;
      }
      else{
        widget[pathItems[4]] = message.payloadString;
      };

      if(pathItems[4] === 'room'){
        widget[pathItems[4]] = message.payloadString;
        var room = data.rooms[message.payloadString];
        if (room == undefined) {
          room = {'widgets' : []};
        }
        room.widgets.push(widget.uid);
      };

      if(pathItems[4] === 'template'){
        widget[pathItems[4]] = message.payloadString;
      };

      if(pathItems[4] === 'order'){
          widget[pathItems[4]] = parseInt(message.payloadString);
      };

      if (pathItems[4] === 'enabled') {
          var value = message.payloadString;
          if (message.payloadString == "1") {
              value = 1;
          } else if (message.payloadString == "0") {
              value = 0;
          }
          widget[pathItems[4]] = value;
      }

      data.widgets[widgetUID] = widget;
    }

    function parseRoomMsg(pathItems, message){
      if(pathItems[4] === 'name'){
        data.rooms[pathItems[3]] = { uid: pathItems[3], name: message.payloadString, widgets: [] };
      };
    }

    function parseDashboardMsg(pathItems, message){
      var dashboardUID = pathItems[3];
      var dashboard = { widgets: {} };

	  //FIXME: properly handle null messages
      if (message.payloadString == "") {
		return;
      }

      if(data.dashboards[dashboardUID] != null){
        dashboard = data.dashboards[dashboardUID];
      } else {
        dashboard['uid'] = dashboardUID;
      }

      if(pathItems[4] === 'widgets'){
        dashboard.widgets[pathItems[5]] = dashboard.widgets[pathItems[5]] || {};
        switch(pathItems[6]) {
          case "uid":
            //~ dashboard.widgets[pathItems[5]]['uid'] = data.widgets[message.payloadString];
            dashboard.widgets[pathItems[5]] = data.widgets[message.payloadString];
            break;
          default:
            // console.log("WARNING: Unknown dashboard message: " + pathItems[6]);
            return null;
            break;
        }
      }
      else{
        dashboard[pathItems[4]] = message.payloadString;
      }

      data.dashboards[dashboardUID] = dashboard;
    }

    function parseParameters(pathItems, message){
        var parameter = pathItems[2];
        data.parameters[parameter] = message;
      }

    return dataService;
  }]);
