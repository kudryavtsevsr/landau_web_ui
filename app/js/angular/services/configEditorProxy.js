"use strict";

angular.module('landauApp')
  .factory("ConfigEditorProxy", [ "MqttRpc", function (MqttRpc) {
    return MqttRpc.getProxy("confed/Editor", [
      "List",
      "Load",
      "Save"
    ], "configEditorProxy");
  }]);
