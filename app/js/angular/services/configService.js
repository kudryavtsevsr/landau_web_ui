"use strict";

angular.module('landauApp')
    .factory("Config", function ($routeParams, $timeout, ConfigEditorProxy, whenMqttReady, gotoDefStart, $location, PageState, errors) {
        var configService = {};

        configService.file = {
            schemaPath: $routeParams.path,
            configPath: "",
            loaded: false,
            valid: true,
            content: {}
        };

        // Для использования нескольких контроллеров на одной странице
        configService.schemaPathSet = function (path) {
            configService.file.schemaPath = path;
        };

        configService.editorOptions = {};
        if (!/^\//.test(configService.file.schemaPath))
            configService.file.schemaPath = "/" + configService.file.schemaPath;

        configService.canSave = function () {
            return PageState.isDirty() && configService.file.valid;
        };

        configService.onChange = function (content, errors) {
            if (!angular.equals(configService.file.content, content)) {
                PageState.setDirty(true);
                configService.file.content = content;
            }
            configService.file.valid = !errors.length;
        };

        var load = function () {
            ConfigEditorProxy.Load({path: configService.file.schemaPath})
                .then(function (r) {
                    configService.editorOptions = r.schema.strictProps ? {no_additional_properties: true} : {};
                    if (r.schema.limited)
                        angular.extend(configService.editorOptions, {
                            disable_properties: true,
                            disable_edit_json: true
                        });
                    configService.file.configPath = r.configPath;
                    configService.file.content = r.content;
                    configService.file.schema = r.schema;
                    configService.file.loaded = true;
                })
                .catch(errors.catch("Error loading the file"));
        };

        configService.save = function () {
            console.log("save!");
            PageState.setDirty(false);
            ConfigEditorProxy.Save({path: configService.file.schemaPath, content: configService.file.content})
                .then(function () {
                    if (configService.file.schema.needReload)
                        load();
                })
                .catch(function (e) {
                    console.log('catch:');
                    console.log(e);
                    PageState.setDirty(true);
                    errors.showError("Error saving " + configService.file.configPath, e);
                });
        };

        whenMqttReady().then(load);

        return configService;
    });
