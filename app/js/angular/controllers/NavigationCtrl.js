'use strict';

angular.module('landauApp')
    .controller('NavigationCtrl', ['$scope', '$timeout', 'CommonCode', function ($scope, $timeout, CommonCode) {

        var failDialogTimeout = null;

        $scope.isConnected = function () {
            // console.log('isConnected');
            var isConnected = CommonCode.isConnected();
            // console.log('isConnected: ', isConnected);
            // if (isConnected) {
            //     $('#fail-connection-dialog').modal('hide');
            //     if(failDialogTimeout !== null) {
            //         failDialogTimeout = null;
            //         console.log('$timeout.cancel');
            //         $timeout.cancel(failDialogTimeout);
            //     }
            // } else {
            //     failDialogTimeout = $timeout(function () {
            //         failDialogTimeout = null;
            //         console.log('$timeout.fire');
            //         $('#fail-connection-dialog').modal('show')
            //     }, 5000);
            // }
            return isConnected;
        };

        var data = CommonCode.data;
        /**
         * Метод для поиска ошибок устройств (датчиков, реле) в виджетах
         * @returns {boolean}
         */
        $scope.controlError = function () {
            // console.log('controlError');
            for (var key in data.widgets) {
                var widget = data.widgets[key];
                // console.log('widget: ', key);
                var controls = widget.controls;
                for (var key2 in controls) {
                    // console.log('control: ', key2);
                    var slot = widget.controls[key2];
                    // console.log('topic: ', slot.topic);
                    if ( slot.topic !== undefined && slot.topic.hasOwnProperty('metaError') && slot.topic.metaError !="") {
                        // console.log('metaError!');
                        return false;
                    }
                }
            }
            return true;
        }
    }]);