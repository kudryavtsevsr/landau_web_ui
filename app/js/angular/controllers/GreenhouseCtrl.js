'use strict';

angular.module('landauApp')
    .controller('GreenhouseCtrl', ['$scope', '$rootScope', '$routeParams', 'CommonCode', 'RuleSettingsProxy', 'errors',  function ($scope, $rootScope, $routeParams, CommonCode, RuleSettingsProxy, errors){
        // Получим все данные из топиков
        $scope.data = CommonCode.data;

        console.log('GreenhouseCtrl $scope.data: ', $scope.data);

        // Создадим отдельные объекты
        $scope.dashboards = $scope.data.dashboards;
        $scope.widgets = $scope.data.widgets;
        $scope.controls = $scope.data.controls;
        $scope.widgetTemplates = $scope.data.widget_templates;
        $scope.widget = { controls: {}, options: {} };
        // Создадим пустой объект для новой панели(теплицы)
        $scope.dashboard = { widgets: {} };
        // Объявление template
        $scope.template = '';

        // Установим UID открытой теплицы
        $scope.dashboardID = $routeParams.id;
        $scope.$watch('dashboards.' + $scope.dashboardID, function(){
            $scope.dashboard = $scope.dashboards[$scope.dashboardID];
        });

        // Заголовок диалогового окна
        $scope.action = '';

        $scope.disableButton = false;

        $scope.$watch('data.widgets',
            function (Value) {
                console.log('data.widgets: ', Value);
                $scope.widgetsOrder = Object.keys(Value);
                console.log('widgetsOrder: ', Object.keys(Value));
            }
        );

        /**
         * В зависимости от выбранного шабона, помещаем в виджет соответствующие controls и options
         */
        $scope.renderFieldsForTemplate = function () {
            $scope.widget.controls = {};
            $scope.widget.options = {};
            if ($scope.template) {
                $scope.widget.controls = $scope.template.slots;
                $scope.widget.options = $scope.template.options;
            }
        };

        /**
         * Добавление или изменение виджета
         */
        $scope.addOrUpdateWidget = function () {
            console.log('Создание виджета');
            $scope.widget.template = $scope.template.uid;

            // Создаём свойство порядка сортировки
            Object.defineProperty($scope.widget, 'order', {
                value: 0, configurable: true, writable: true, enumerable: true
            });

            // Если создаётся новый виджет
            if (!$scope.widget.uid) {
                // Создаём uid для нового виджета
                var max_uid_index = 0;
                for (var key in $scope.widgets) {
                    var uid_index = parseInt(key.slice("widget".length));
                    if (uid_index > max_uid_index) {
                        max_uid_index = uid_index;
                    }
                }
                $scope.widget.uid = "widget" + (max_uid_index + 1);

                // Если создаваемый виджет - виджет правила
                if ($scope.detectRule($scope.widget.template)) {
                    // Добавляем к правилу свойство активности
                    Object.defineProperty($scope.widget, 'enabled', {
                        value: 0, configurable: true, writable: true, enumerable: true
                    });
                    // Добавляем к правилу уникальное имя
                    var rule_name = findRuleName($scope.widget.template, 0, $scope.widget.template);
                    Object.defineProperty($scope.widget, 'ruleName', {
                        value: rule_name, configurable: true, writable: true, enumerable: true
                    });
            }
            }

            // Объявляем основной топик виджета
            var topic = '/config/widgets/' + $scope.widget.uid;
            $scope.mqtt_widget = angular.copy($scope.widget);

            // Записываем в объект виджета контролы
            for (var c in $scope.mqtt_widget.controls) {
                var control = $scope.mqtt_widget.controls[c];
                $scope.mqtt_widget.controls[control.uid] = {uid: control.uid, topic: control.topic.topic};
            }

            // Посылаем запрос на добавление виджета в топики
            $scope.mqttSendCollection(topic, $scope.mqtt_widget);
            console.log('Виджет создан');

            // Добавляем виджет в открытую теплицу
            if ($scope.action == 'add') {
                console.log('Добавление виджета в теплицу');
                $scope.dashboard.widgets[$scope.mqtt_widget.uid] = $scope.mqtt_widget;
                console.log('dashboard', $scope.dashboard);
                topic = '/config/dashboards/' + $scope.dashboard.uid;
                $scope.mqttSendCollection(topic, $scope.dashboard);
                console.log('Виджет добавлен в теплицу');
            }

            // Закрываем все модальные окна и очищаем переменные
            $('.modal').modal('hide');
            $scope.widget = {controls: {}, options: {}};

        };

        /**
         * По нажатию на кнопку запускает или останавливает правило
         * @param trigger
         * @param widget
         */
        $scope.startRuleOrStopRule = function(trigger, widget) {
            $scope.disableButton = true;
            if (!trigger) {
                startRule(widget);
            } else {
                stopRule(widget);
            }
        };

        /**
         * Установка виджета для редактирования
         */
        $scope.editWidgetAction = function (widget) {
            $scope.action = 'edit';
            $scope.widget = widget;
            $scope.widgetID = widget.uid;
            $scope.$watch('widgets.' + $scope.widgetID, function () {
                $scope.widget = $scope.widgets[$scope.widgetID];
                if ($scope.widget) {
                    $scope.$watch('widget.template', function () {
                        $scope.$watch('widgetTemplates.' + $scope.widget.template, function () {
                            $scope.template = $scope.widgetTemplates[$scope.widget.template];
                        });
                    });
                }
            });
        };

        /**
         * При нажатии на кнопку добавления нового виджетов
         */
        $scope.addWidgetAction = function(){
            // Обнуляем виджет
            $scope.widget = { controls: {}, options: {} };
            $scope.template = {};
            $scope.action = 'add';
        };

        /**
         * Удаление виджета
         */
        $scope.deleteWidget = function (widget) {
            CommonCode.deleteWidget(widget);
            // Закрываем все модальные окна и очищаем переменные
            $('.modal').modal('hide');
            $scope.widget = { controls: {}, options: {} };
        };

        /**
         * Определяет для правила шаблон или нет
         * @param template Шаблон
         */
        $scope.detectRule = function (template) {
            return /_rule/.test(template);
        };

        /**
         * Функция активации правила
         * @param widget
         */
        function startRule (widget) {
            // Определяем тип правила
            var rule_type = widget.template;
            var parameters = {};

            // Формируем настройки правила
            switch (rule_type) {
                case "air_humidity_rule":
                    parameters = {
                        "rule_type": rule_type,
                        "rule_name": widget.ruleName,
                        "uid": widget.uid,

                        "air_humidity_max": widget.options.option2.value,
                        "air_humidity_min": widget.options.option1.value,
                        "air_humidity_sensor": parseParams(widget.controls.slot1.topic.topic),
                        "air_dehumidifier": parseParams(widget.controls.slot2.topic.topic),
                        "air_humidifier": parseParams(widget.controls.slot3.topic.topic)
                    };
                    break;
                case "air_temperature_rule":
                    parameters = {
                        "rule_type": rule_type,
                        "rule_name": widget.ruleName,
                        "uid": widget.uid,

                        "air_temperature_max": widget.options.option2.value,
                        "air_temperature_min": widget.options.option1.value,
                        "air_temperature_sensor": parseParams(widget.controls.slot1.topic.topic),

                        "air_humidity_max": widget.options.option4.value,
                        "air_humidity_min": widget.options.option3.value,
                        "air_humidity_sensor": parseParams(widget.controls.slot5.topic.topic),

                        "air_cooler_fan": parseParams(widget.controls.slot3.topic.topic),
                        "air_cooler_fogger": parseParams(widget.controls.slot4.topic.topic),
                        "air_heater": parseParams(widget.controls.slot2.topic.topic)
                    };
                    break;
                case "illumination_by_sensor_rule":
                    parameters = {
                        "rule_type": rule_type,
                        "rule_name": widget.ruleName,
                        "uid": widget.uid,

                        "illumination_min": widget.options.option1.value,
                        "illumination_time_start": [widget.options.option2.value1, widget.options.option2.value2],
                        "illumination_time_end": [widget.options.option3.value1, widget.options.option3.value2],
                        "illumination_sensor": parseParams(widget.controls.slot1.topic.topic),
                        "illumination": parseParams(widget.controls.slot2.topic.topic)
                    };
                    break;
                case "soil_moisture_rule":
                    parameters = {
                        "rule_type": rule_type,
                        "rule_name": widget.ruleName,
                        "uid": widget.uid,

                        "soil_moisture_max": widget.options.option2.value,
                        "soil_moisture_min": widget.options.option1.value,
                        "soil_moisture_sensor": parseParams(widget.controls.slot1.topic.topic)
                    };
                    break;
                case "soil_temperature_rule":
                    parameters = {
                        "rule_type": rule_type,
                        "rule_name": widget.ruleName,
                        "uid": widget.uid,

                        "soil_temperature_max": widget.options.option2.value,
                        "soil_temperature_min": widget.options.option1.value,
                        "soil_temperature_sensor": parseParams(widget.controls.slot1.topic.topic),
                        "soil_heater": parseParams(widget.controls.slot2.topic.topic)
                    };
                    break;
                case "watering_by_schedule_rule":
                    parameters = {
                        "rule_type": rule_type,
                        "rule_name": widget.ruleName,
                        "uid": widget.uid,

                        "water_volume_liters": widget.options.option1.value,
                        "irrigation_efficiency": widget.options.option2.value,
                        "watering_start": {
                            "year": widget.options.option3.value3,
                            "month": parseInt(widget.options.option3.value2) - 1,
                            "day": widget.options.option3.value1
                        },
                        "watering_end": {
                            "year": widget.options.option4.value3,
                            "month": parseInt(widget.options.option4.value2) - 1,
                            "day": widget.options.option4.value1
                        },
                        "watering_time": [parseInt(widget.options.option5.value1) - 3, widget.options.option5.value2],
                        "watering_frequency": widget.options.option6.value,
                        "watering_relay": parseParams(widget.controls.slot1.topic.topic)
                    };
                    break;
                default:
                    console.log('Unknown rule');
            }

            // Отправляем rpc запрос на активацию правила
            RuleSettingsProxy.addScript(
                {
                    a: parameters
                }
            )
                .then(function (reply) {
                    $scope.disableButton = false;
                    console.log('addScript result: ' + reply);
                    if(reply[0]){
                        // mqttClient.send('/config/widgets/' + widget.ruleName + '/enabled', '1', true);
                    } else {
                        errors.showError("При активации правила произошла ошибка", reply[1]);
                    }
                })
                .catch(function (e) {
                    $scope.disableButton = false;
                    errors.showError("При активации правила произошла ошибка", e);
                });
        }

        /**
         * Функция остановки правила
         * @param widget
         */
        function stopRule (widget) {
            // Формируем параметры для правила
            var rule_name = widget.ruleName,
                uid =  widget.uid;
            // Отправляем rpc запрос на активацию правила
            RuleSettingsProxy.delScript(
                {
                    a: rule_name,
                    b: uid
                }
            )
                .then(function (reply) {
                    $scope.disableButton = false;
                    console.log('delScript result: ' + reply);
                    if(reply[0]){
                        // mqttClient.send('/config/widgets/' + widget.ruleName + '/enabled', '0', true);
                    } if(!reply[0]) {
                        errors.showError("При деактивации правила произошла ошибка", reply[1]);
                    }
                })
                .catch(function (e) {
                    $scope.disableButton = false;
                    errors.showError("При деактивации правила произошла ошибка", e);
                });
        }

        /**
         * Рекурсивный поиск не занятого имени правила среди существующих правил
         */
        function findRuleName (template, index, template_base) {
            console.log('template: ' , template);
            var name_new = template;
            for (var key in $scope.widgets) {
                var widget = $scope.widgets[key];
                console.log('widget.ruleName: ', widget.ruleName);
                if (name_new === widget.ruleName) {
                    console.log(name_new + ' == ' + widget.ruleName);
                    index++;
                    template = findRuleName(template_base + String(index), index, template_base);
                }
            }
            return template;
        }

        /**
         * Рекурсивный поиск не занятого имени правила среди виртуальных устройств
         */
        function getTopicString (template, index, template_base){
            console.log('template: ' , template);
            var topic_new = "/devices/" + template + "/controls/enabled";
            for (var key in $scope.controls) {
                var topic = $scope.controls[key].topic;
                console.log('topic: ', topic);
                if (topic_new === topic) {
                    console.log(topic_new + ' == ' + topic);
                    index++;
                    template = getTopicString(template_base + String(index), index, template_base);
                }
            }
            return template;
        }

        /**
         * Возвращает строку в формате "device/control" из строки топика
         */
        function parseParams(topic) {
            var deviceInfo = topic.split('/');
            var deviceName = deviceInfo[2];
            var controlName = deviceInfo[4];
            return deviceName + '/' + controlName;
        }

        /**
         * Возвращает имя устройства по топику
         */
        function parseDevice(topic){
            var deviceInfo = topic.split('/');
            return deviceInfo[2];
        }

    }])
    .directive('widgetTemplates', function(){
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'views/widgets/widget-templates.html'
        };
    })

    .directive('widgetHumidity', function () {
        return {
            restrict: 'A',
            replace: true,
            templateUrl: 'views/widgets/widget-humidity.html'
        };
    })
    .directive('widgetTemperature', function () {
        return {
            restrict: 'A',
            replace: true,
            templateUrl: 'views/widgets/widget-temperature.html'
        };
    })
    .directive('widgetSwitch', function () {
        return {
            restrict: 'A',
            replace: true,
            templateUrl: 'views/widgets/widget-switch.html'
        };
    })
    .directive('widgetAirHumidityRule', function(){
        return {
            restrict: 'A',
            replace: true,
            templateUrl: 'views/widgets/widget-air_humidity_rule.html'
        };
    })
    .directive('widgetAirTemperatureRule', function(){
        return {
            restrict: 'A',
            replace: true,
            templateUrl: 'views/widgets/widget-air_temperature_rule.html'
        };
    })
    .directive('widgetIlluminationBySensorRule', function(){
        return {
            restrict: 'A',
            replace: true,
            templateUrl: 'views/widgets/widget-illumination_by_sensor_rule.html'
        };
    })
    .directive('widgetSoilMoistureRule', function(){
        return {
            restrict: 'A',
            replace: true,
            templateUrl: 'views/widgets/widget-soil_moisture_rule.html'
        };
    })
    .directive('widgetSoilTemperatureRule', function(){
        return {
            restrict: 'A',
            replace: true,
            templateUrl: 'views/widgets/widget-soil_temperature_rule.html'
        };
    })
    .directive('widgetWateringByScheduleRule', function(){
        return {
            restrict: 'A',
            replace: true,
            templateUrl: 'views/widgets/widget-watering_by_schedule_rule.html'
        };
    })

    .directive('widgetDialog', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'views/widgets/widget-dialog.html'
        };
    });