'use strict';

angular.module('landauApp')
    .controller('GreenhousesCtrl', ['$scope', '$rootScope', 'CommonCode', '$timeout', function ($scope, $rootScope, CommonCode, $timeout){
        // Получим все данные из топиков
        $scope.data = CommonCode.data;
        console.log('GreenhousesCtrl $scope.data: ', $scope.data);

        // Создадим отдельные объекты для панелей(теплиц) и виджетов
        $scope.dashboards = $scope.data.dashboards;
        $scope.widgets = $scope.data.widgets;
        // Создадим пустой объект для новой панели(теплицы)
        $scope.dashboard = { widgets: {} };
        // Создадим объект теплицы для шаблона
        $scope.greenhouse = {};

        // Применение masonry к виджетам теплиц
        var $grid = null;
        $scope.$watch(
            function () {
                return $('.grid').children().length;
            },
            function (newValue, oldValue) {
                if (newValue !== oldValue) {
                    if ($grid != null) $grid.masonry('destroy');
                    $grid = $('.grid').masonry({
                        itemSelector: '.grid-item'
                    });
                }
            }
        );

        /**
         * Метод для создания или обновления теплицы
         */
        $scope.addOrUpdateGreenhouse = function(){
            console.log('Создание теплицы...');
            console.log('Name: ', $scope.greenhouse.name);
            // В шаблоне модель объявлена как greenhouse, прередадим её имя в dashboard
            $scope.dashboard.name = $scope.greenhouse.name;

            // Если в scope нет uid, создадим, учтя все существующие панели
            if (!$scope.dashboard.uid) {
                var max_uid_index = 0;
                for (var key in $scope.dashboards) {
                    var  uid_index = parseInt(key.slice("dashboard".length));
                    if (uid_index > max_uid_index) {
                        max_uid_index = uid_index;
                    }
                }
                $scope.dashboard.uid = "dashboard" + (max_uid_index + 1);
            }

            var topic = '/config/dashboards/' + $scope.dashboard.uid;

            var dashboard = $scope.dashboard;

            for(var w in dashboard.widgets){
                var widget = dashboard.widgets[w];
                if (widget == null) {
                    delete dashboard.widgets[w];
                    $scope.mqttDeleteByPrefix( topic + '/widgets/' + w + '/');
                } else {
                    dashboard.widgets[w] = widget;
                }
            }

            $scope.mqttSendCollection(topic, dashboard);

            console.log('Теплица создана');

            // Закрываем все модальные окна и очищаем переменные
            $('.modal').modal('hide');
            $scope.greenhouse.name = '';
            $scope.dashboard = { widgets: {} };
        };

        /**
         * Установить теплицу для редактирования
         */
        $scope.setGreenhouseForEdit = function(dashboard){
            $scope.greenhouse.name = dashboard.name;
            $scope.dashboard = dashboard;
        };

        /**
         * Удалить теплицу
         */
        $scope.deleteGreenhouse = function (dashboard) {
            // Проходим по всем панелям
            for (var key in $scope.dashboards) {
                // Находим удаляемую панель
                if ($scope.dashboards[key] == dashboard) {
                    var uid = dashboard.uid;
                    delete $scope.dashboards[key];
                    // Посылавем запрос на удаление
                    $scope.mqttDeleteByPrefix('/config/dashboards/' + uid + '/');
                    // Если удаляемая панель стоит по умолчанию
                    if ($scope.data.defaults.dashboard == uid) {
                        // Убираем панель по умолчанию
                        $scope.data.defaults.dashboard = '';
                        mqttClient.send('/config/default_dashboard/uid', '');
                    }
                }
            }
            // Закрываем все модальные окна
            $('.modal').modal('hide');
        };

    }]);